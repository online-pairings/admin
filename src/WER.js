import React from 'react';
import { required, Edit, Create, SimpleForm, Filter, FileInput, List, FileField, Datagrid, TextField, DateField, DisabledInput } from 'react-admin';


const WERFilter = (props) => (
    <Filter {...props}>
        <TextField label="id" source="id" alwaysOn />
    </Filter>
);


export const WERList = props => (
    <List {...props} filters={<WERFilter />}>
        <Datagrid rowClick="edit">
            <TextField source="id" />
            <DateField source="created" />
        </Datagrid>
    </List>
);

export const WEREdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <DisabledInput source="title" />
            <FileInput source="raw" label="WER export" accept=".wer" validate={required()}>
                <FileField source="src" title="title" />
            </FileInput>
            <DisabledInput source="created" />
        </SimpleForm>
    </Edit>
);

export const WERCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <FileInput source="raw" label="WER export" accept=".wer" validate={required()}>
                <FileField source="src" title="title" />
            </FileInput>
        </SimpleForm>
    </Create >
);
import React from 'react';
import { Admin, Resource, ShowGuesser } from 'react-admin';
import WERIcon from '@material-ui/icons/Book';
import { WEREdit, WERCreate, WERList } from './WER';
import backendProvider from './backendProvider';
import addUploadFeature from './addUploadFeature';

const eventsProvider = addUploadFeature(backendProvider('/backend/admin'));
const App = () => (
  <Admin title="Pairings Online • Admin" dataProvider={eventsProvider}>
    <Resource name="wer" create={WERCreate} list={WERList} show={ShowGuesser} edit={WEREdit} icon={WERIcon} />
  </Admin>
);

export default App;
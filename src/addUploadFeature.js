const readFile = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsText(file.rawFile);

    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
});

const addUploadFeature = requestHandler => (type, resource, params) => {
    if ((type === 'UPDATE' || type === 'CREATE') && resource === 'wer') {
        if (params.data.raw && params.data.raw.rawFile instanceof File) {
            return readFile(params.data.raw)
                .then(text => requestHandler(type, resource, {
                    ...params,
                    data: {
                        ...params.data,
                        raw: text,
                        title: params.data.raw.title
                    },
                }));
        }
    }
    return requestHandler(type, resource, params);
};

export default addUploadFeature;